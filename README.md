Pet game project aiming to showing gameplay mechanics such as:

- Procedurally generated level
- Third person character with swift movements
- Smooth camera work
- Enemies and AI
- Swift and rewarding combat

` `
```
.,__,.........,__,.....╭¬¬¬¬¬━━╮
`•.,¸,.•*¯`•.,¸,.•*|:¬¬¬¬¬¬::::|:^-----^
`•.,¸,.•*¯`•.,¸,.•*|:¬¬¬¬¬¬::::||｡◕‿‿◕｡|
-........--""-.......--"╰O━━━━O╯╰--O-O--╯
```
