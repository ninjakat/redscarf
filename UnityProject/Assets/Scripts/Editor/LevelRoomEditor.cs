﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelRoom))]
public class LevelRoomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LevelRoom room = (LevelRoom)target;
        if (GUILayout.Button("Expand"))
        {
            room.Expand();
        }
    }
}
