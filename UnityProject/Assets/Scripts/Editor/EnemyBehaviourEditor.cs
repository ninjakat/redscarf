﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemyBehaviour))]
public class EnemyBehaviourEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EnemyBehaviour enemy = (EnemyBehaviour)target;

        if (GUILayout.Button("Attack"))
        {
            enemy.Attack();
        }
        else if (GUILayout.Button("TakeDamage"))
        {
            enemy.OnDamageTaken(null, 1);
        }
        else if (GUILayout.Button("Die"))
        {
            DamageComponent dmgComp;
            if ((dmgComp = enemy.GetComponent<DamageComponent>()) != null)
            {
                dmgComp.Health = 0;
            }

            enemy.OnDeath();
        }
        else if (GUILayout.Button("Reset"))
        {
            enemy.Reset();
        }
    }
}
