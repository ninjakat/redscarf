﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour
{
    [Header("Patrol Behaviour")]
    public EnemyPatrolArea patrolArea;
    public float patrolMinDistance = 3.0f;
    public float patrolPointReachDistance = 0.3f;
    public float patrolIdleTime = 1.0f;
    public float patrolIdleTimeVariance = 1.0f;
    public float patrolInvalidPointIdleTime = 1.0f;

    [Header("Sensors")]
    public float sightDistance = 15.0f;
    public float sightAngle = 60.0f;

    [Header("Attack")]
    public float shootAngle = 1.0f;
    public GameObject projectile;
    public Transform projectileSpawn;

    int disableAttackTagHash = Animator.StringToHash("DisableAttack");
    const float playerHeight = 1.8f;

    bool playerInSight;
    float distanceToPlayer;
    float angleToPlayer;

    NavMeshAgent agent;
    Animator animator;
    State currentState;

    GameObject player;

    Vector3 previousPosition;

    Vector3 patrolDestination;
#if UNITY_EDITOR
    bool patrolFailedDestination;
#endif
    float idleTimeLeft = 0.0f;

    DamageComponent damageComponent;


    enum State
    {
        Idle,
        Patrol,
        AttackNear,
        AttackFar
    }

    private void OnEnable()
    {
        if (damageComponent)
        {
            damageComponent.TakeDamage = TakeDamage;

            damageComponent.OnDeath += OnDeath;
            damageComponent.OnDamageTaken += OnDamageTaken;
        }
    }

    private void OnDisable()
    {
        if (damageComponent)
        {
            damageComponent.OnDeath -= OnDeath;
            damageComponent.TakeDamage -= TakeDamage;
        }
    }

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        damageComponent = GetComponent<DamageComponent>();
    }

    private void Start()
    {
        if (patrolArea == null)
        {
            Debug.LogWarning(gameObject.name + " doesn't have a patrol area");
        }

        if (agent == null)
        {
            Debug.LogWarning(gameObject.name + " doesn't have a nav mesh agent");
        }

        if (animator == null)
        {
            Debug.LogWarning(gameObject.name + " doesn't have an animator");
        }

        Patrol();

        previousPosition = transform.position;
    }

    void Update()
    {
        if (damageComponent && damageComponent.isDead)
        {
            return;
        }

        UpdateSensors();

        if (playerInSight)
        {
            Attack();
        }

        // Update state
        switch (currentState)
        {
            case State.Idle:
            {
                // Done waiting?
                idleTimeLeft -= Time.deltaTime;
                if (idleTimeLeft <= 0.0f)
                {
                    // Resume patrolling
                    Patrol();
                }
            }
            break;

            case State.Patrol:
            {
                // Reached target?
                if (agent.remainingDistance < patrolPointReachDistance)
                {
                    // Idle a bit
                    Idle(patrolIdleTime + Random.Range(-patrolIdleTimeVariance, patrolIdleTimeVariance));
                }
            }
            break;

            case State.AttackNear:
            break;

            case State.AttackFar:
            {
                OrientToPlayer();
                if (Mathf.Abs(angleToPlayer) < shootAngle * 0.5f && CanAttack())
                {
                    Shoot();
                }
            }
            break;
        }

        // Update animator
        if (agent != null && animator != null)
        {
            float currentSpeed = (transform.position - previousPosition).magnitude / Time.deltaTime;
            animator.SetFloat("normalizedSpeed", currentSpeed / agent.speed);
        }
        previousPosition = transform.position;
    }

    void UpdateSensors()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        if (player != null)
        {
            Vector3 playerPos = player.transform.position;
            Vector3 vectorToPlayer = playerPos - transform.position;

            distanceToPlayer = vectorToPlayer.magnitude;
            angleToPlayer = Mathf.Rad2Deg * Mathf.Asin(Vector3.Dot(transform.right, vectorToPlayer.normalized));

            playerInSight = false;
            if (distanceToPlayer < sightDistance && Mathf.Abs(angleToPlayer) < sightAngle * 0.5f)
            {
                RaycastHit hitInfo;
                if (Physics.Raycast(transform.position, vectorToPlayer, out hitInfo, sightDistance))
                {
                    if (hitInfo.collider.gameObject.tag == "Player")
                    {
                        playerInSight = true;
                    }
                }

            }
        }
    }

    void OrientToPlayer()
    {
        Debug.Assert(player != null);
        Debug.Assert(agent != null);

        float rotDirection = Mathf.Sign(angleToPlayer);
        float rotMagnitude = Mathf.Abs(angleToPlayer);
        rotMagnitude = Mathf.Min(rotMagnitude, agent.angularSpeed * Time.deltaTime);

        transform.rotation *= Quaternion.Euler(0, rotDirection * rotMagnitude, 0);
    }

    void Patrol()
    {
#if UNITY_EDITOR
        patrolFailedDestination = false;
#endif

        // Try finding our next patrol point
        if (patrolArea != null)
        {
            Vector3? patrolPoint = patrolArea.GetRandomNavPoint();
            if (patrolPoint != null && (patrolPoint.Value - transform.position).magnitude > patrolMinDistance)
            {
                // Got patrol point, go there
                patrolDestination = patrolPoint.Value;
                agent.SetDestination(patrolDestination);
                currentState = State.Patrol;
                return;
            }
#if UNITY_EDITOR
            else
            {
                if (patrolPoint != null)
                {
                    // Still remember the patrol point so we can debug draw it
                    patrolFailedDestination = true;
                    patrolDestination = patrolPoint.Value;
                }
            }
#endif
        }

        // No good patrol point found, idle a bit more
        Idle(patrolInvalidPointIdleTime);
    }

    void Idle(float time)
    {
        idleTimeLeft = time;
        currentState = State.Idle;
    }

    public void Attack()
    {
        if (agent != null)
        {
            agent.isStopped = true;
        }

        currentState = State.AttackFar;
    }

    bool CanAttack()
    {
        if (animator != null)
        {
            AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(0);
            AnimatorStateInfo nextState = animator.GetNextAnimatorStateInfo(0);

            if (currentState.tagHash != disableAttackTagHash && nextState.tagHash != disableAttackTagHash)
            {
                return true;
            }
        }

        return false;
    }

    public void Shoot()
    {
        // Play anim that will trigger AttackFrame()
        if (animator != null)
        {
            animator.SetTrigger("attack");
        }
    }

    public void AttackFrame()
    {
        Debug.Assert(player != null);

        // Check we have something to spawn and somehwere to spawn it
        if (projectile == null)
        {
            Debug.LogError(gameObject.name + " is trying to shoot but projectile is null");
            return;
        }
        else if (projectileSpawn == null)
        {
            Debug.LogError(gameObject.name + " is trying to shoot but projectileSpawn is null");
            return;
        }

        // Shoot foward, with adjusted pitch
        Vector3 targetPosition = player.transform.position + playerHeight * 0.5f * Vector3.up;
        Vector3 directionToTarget = (targetPosition - projectileSpawn.position).normalized;
        float pitchToTarget = Mathf.Rad2Deg * Mathf.Asin(-directionToTarget.y);
        float yawToTarget = projectileSpawn.rotation.eulerAngles.y;

        GameObject.Instantiate(projectile, projectileSpawn.position, Quaternion.Euler(pitchToTarget, yawToTarget, 0));
    }

    float TakeDamage(GameObject other, float damage)
    {
        return damage;
    }

    public void OnDamageTaken(GameObject causer, float damage)
    {
        // React to the damage
        if (damage > 0)
        {
            if (animator != null)
            {
                animator.SetTrigger("damaged");
            }
        }
    }

    public void OnDeath()
    {
        if (animator != null)
        {
            animator.SetTrigger("die");
        }

        if (agent != null)
        {
            agent.isStopped = true;
        }
    }

    public void Reset()
    {
        if (animator != null)
        {
            animator.SetTrigger("reset");
        }

        Start();
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        // Patrol
        float width = patrolPointReachDistance * 2.0f;
        if (patrolFailedDestination)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(patrolDestination, new Vector3(width, 2.0f, width));
        }
        else if (agent != null)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(agent.destination, new Vector3(width, 2.0f, width));
        }

        // View cone
        Matrix4x4 m = new Matrix4x4();
        m.SetTRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = m;
        Gizmos.color = Color.red;

        Vector3 sightVector = Vector3.forward * sightDistance;
        Gizmos.DrawRay(Vector3.zero, Quaternion.Euler(0, -sightAngle * 0.5f, 0) * sightVector);
        Gizmos.DrawRay(Vector3.zero, Quaternion.Euler(0, sightAngle * 0.5f, 0) * sightVector);
        Gizmos.DrawLine(Quaternion.Euler(0, -sightAngle * 0.5f, 0) * sightVector, Quaternion.Euler(0, -sightAngle * 0.25f, 0) * sightVector);
        Gizmos.DrawLine(Quaternion.Euler(0, -sightAngle * 0.25f, 0) * sightVector, Quaternion.Euler(0, -sightAngle * 0.0f, 0) * sightVector);
        Gizmos.DrawLine(Quaternion.Euler(0, sightAngle * 0.0f, 0) * sightVector, Quaternion.Euler(0, sightAngle * 0.25f, 0) * sightVector);
        Gizmos.DrawLine(Quaternion.Euler(0, sightAngle * 0.25f, 0) * sightVector, Quaternion.Euler(0, sightAngle * 0.5f, 0) * sightVector);
    }
#endif
}