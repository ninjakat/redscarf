﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPatrolArea : MonoBehaviour
{
    public float radius = 5.0f;

    int navAreaMask;

    private void Awake()
    {
        navAreaMask = 1 << NavMesh.GetAreaFromName("Walkable");
    }

    // Returns a random point on the nav mesh in a circle around the game object
    public Vector3? GetRandomNavPoint()
    {
        // Get world radius of the area
        Vector3 scale = transform.lossyScale;
        float worldRadius = radius * Mathf.Max(Mathf.Max(scale.x, scale.y), scale.z);

        // Create a random offset within the area circle
        float angle = Random.Range(0, 360);
        float size = Random.Range(0, worldRadius);
        Vector3 randomOffset = Quaternion.Euler(0, angle, 0) * Vector3.forward * size;

        // Try sampling the nav mesh
        NavMeshHit navHit;
        if (NavMesh.SamplePosition(transform.position + randomOffset, out navHit, worldRadius, navAreaMask))
        {
            return navHit.position;
        }

        return null;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0.0f, 0.5f, 1.0f);

        Vector3 scale = transform.lossyScale * radius;
        float size = Mathf.Max(Mathf.Max(scale.x, scale.y), scale.z);
        Gizmos.DrawWireSphere(transform.position, size);
    }
}
