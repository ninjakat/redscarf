﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float walkingSpeed = 8.0f;
    public float attackWalkingSpeed = 3.0f;
    public float rotationSpeed = 360.0f;
    public float attackrotationSpeed = 50.0f;

    bool isMoving = false;
    bool isParrying = false;
    bool triggerAttack = false;
    Animator animator;

    int animParryHash = Animator.StringToHash("parry");
    int animAttackHash = Animator.StringToHash("attack");
    int animUpperBodyLayer = 0;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        animUpperBodyLayer = animator.GetLayerIndex("UpperBody");
    }

    void Start()
    {
        DamageComponent dmgCmp = GetComponentInParent<DamageComponent>();
        dmgCmp.TakeDamage = TakeDamage;
    }

    private void Update()
    {
        HandleMovementInputs();
        HandleCombatInputs();

        UpdateAnimations();
    }

    public float TakeDamage(GameObject causer, float damage)
    {
        AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(animUpperBodyLayer);

        if (currentState.tagHash == animParryHash)
        {
            return 0;
        }

        return damage;
    }

    private void UpdateAnimations()
    {
        animator.SetBool("parrying", isParrying);
        animator.SetBool("moving", isMoving);

        // Use a bool instead of a trigger, because we want it true for just one frame
        animator.SetBool("attack", triggerAttack);
    }

    private void HandleCombatInputs()
    {
        isParrying = Input.GetButton("Parry");
        triggerAttack = Input.GetButtonDown("Attack");
    }

    private void HandleMovementInputs()
    {
        bool attacking = (animator.GetCurrentAnimatorStateInfo(animUpperBodyLayer).tagHash == animAttackHash);

        float leftStickVertical = Input.GetAxis("Vertical");
        float leftStickHorizontal = Input.GetAxis("Horizontal");

        Vector3 prevPosition = transform.position;

        // Get the camera forward vector projected on the horizontal plane
        Vector3 forward = Camera.main.transform.forward;
        forward.y = 0;
        forward.Normalize();

        // Derive the Camera right vector
        Vector3 right = new Vector3(forward.z, 0, -forward.x);

        // Apply movement
        Vector3 movement = (leftStickVertical * forward + leftStickHorizontal * right).normalized;
        float currentWalkingSpeed = attacking ? attackWalkingSpeed : walkingSpeed;
        transform.position += movement * currentWalkingSpeed * Time.deltaTime;

        // Update internal status
        isMoving = (movement.sqrMagnitude > 0.1f);

        // Orient the player if needed
        if (isMoving || isParrying)
        {
            float currentYaw = transform.rotation.eulerAngles.y;

            float targetYaw;
            if (isParrying && !triggerAttack)
            {
                targetYaw = Camera.main.transform.rotation.eulerAngles.y;
            }
            else
            {
                targetYaw = Mathf.Rad2Deg * Mathf.Atan2(movement.x, movement.z);
            }

            // Compute the delta yaw between where we are and where the player wants to be
            float deltaYaw = Mathf.Repeat(targetYaw - currentYaw, 360.0f);
            if (deltaYaw > 180.0f)
            {
                deltaYaw -= 360.0f;
            }

            // If we're attacking and moving, snap to that direction, otherwise smooth rotation
            if (!(isMoving && triggerAttack))
            {
                float maxRot = (attacking ? attackrotationSpeed : rotationSpeed) * Time.deltaTime;
                deltaYaw = Mathf.Clamp(deltaYaw, -maxRot, maxRot);
            }

            transform.rotation *= Quaternion.Euler(0, deltaYaw, 0);
        }
    }
}
