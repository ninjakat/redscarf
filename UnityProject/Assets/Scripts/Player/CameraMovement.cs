﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    // What we're trying to follow
    public Transform target;

    // Desired pitch when the camera automatically aligns behind the player
    public float idealPitch = -30.0f;

    // Distance at which the camera should be from the player for a given pitch
    public AnimationCurve idealDistanceForPitch = new AnimationCurve();

    // Multiplies the idealDistanceForPitch curve
    public float distanceFactor = 3.0f;

    // How much does the camera lags behind its target position
    [Range(0.0f, 1.0f)]
    public float cameraTranslationLag = 0.9f;
    // How much does the camera lags behind its target rotation
    [Range(0.0f, 1.0f)]
    public float cameraRotationLag = 0.9f;

    // How much does the camera tries to align when the player is moving
    [Range(0.0f, 1.0f)]
    public float motionAlignTargetLag = 0.9f;

    // How much can the player wiggle around before the camera tries to align
    [Range(0.0f, 1.0f)]
    public float motionAlignDirectionSmooth = 0.95f;

    // Min / Max camera pitch
    public Vector2 pitchRange = new Vector2(-80, 80);

    // Should we lock and hide the mouse
    public bool mouseLock = false;

    public float camSensitivity = 200.0f;

    // Max time after a player camera input until we try to align the camera
    public float inputEffectiveTime = 1.0f;


    Vector3 previousPlayerPosition;
    Vector3 currentTargetPosition;
    Vector3 currentTargetRotation;
    Vector3 averagePlayerDirection;

    // How much time left for the camera input to override the auto-align mechanism
    float inputOverride = 0.0f;

    void Start()
    {
        Debug.Assert(target != null);

        // First frame, pretend the player just started advancing
        previousPlayerPosition = target.position - target.forward;
        averagePlayerDirection = target.forward;

        // Mouse lock
        if (mouseLock)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    void Update()
    {
        Vector3 newPlayerPosition = target.position;

        // Get the rotation we were at the previous frame
        Vector3 targetToPlayer = (previousPlayerPosition - currentTargetPosition).normalized;
        Vector2 prevRotation;
        prevRotation.x = Mathf.Rad2Deg * Mathf.Asin(-targetToPlayer.y);
        prevRotation.y = Mathf.Rad2Deg * Mathf.Atan2(targetToPlayer.x, targetToPlayer.z);

        Vector2 desiredRotation = prevRotation;

        // Handle inputs
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            camSensitivity += 10;
            Debug.Log("Camera sensitivity: " + camSensitivity);
        }
        else if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            camSensitivity -= 10;
            Debug.Log("Camera sensitivity: " + camSensitivity);
        }

        float deltaPitch = Input.GetAxis("RightVertical") * camSensitivity * Time.deltaTime;
        float deltaYaw = Input.GetAxis("RightHorizontal") * camSensitivity * Time.deltaTime;
        if (deltaYaw != 0 || deltaPitch != 0)
        {
            inputOverride = 1.0f;

            desiredRotation = prevRotation + new Vector2(deltaPitch, deltaYaw);
        }

        // Is player moving?
        Vector3 playerSpeed = (newPlayerPosition - previousPlayerPosition) / Time.deltaTime;
        if (playerSpeed.sqrMagnitude > 0.1f)
        {
            // Average the player's direction of movement
            averagePlayerDirection = Vector3.Lerp(averagePlayerDirection, playerSpeed.normalized, (1.0f - motionAlignDirectionSmooth));

            // Can we try to align automatically?
            if (inputOverride == 0.0f)
            {
                // Calculate the offset to be placed behind the average direction of movement
                float idealYaw = Mathf.Rad2Deg * Mathf.Atan2(averagePlayerDirection.x, averagePlayerDirection.z);
                Vector2 idealRotation = new Vector2(idealPitch, idealYaw);

                // How much is the player changing direction
                float movementDirectionality = Vector3.Dot(playerSpeed.normalized, averagePlayerDirection) * 0.5f + 0.5f;

                // How much is the player going othorgonally to the camera
                // (mainly used to avoid rotating the camera if the player walks directly towards it)
                Vector3 playerHSpeed = new Vector3(playerSpeed.x, 0, playerSpeed.z);
                Vector3 targetHOffset = new Vector3(targetToPlayer.x, 0, targetToPlayer.z);
                float camDirectionality = 1.0f - Mathf.Abs(Vector3.Dot(playerHSpeed.normalized, targetHOffset.normalized));

                // Smoothly move the target position towards that offset
                float alpha = (1.0f - motionAlignTargetLag) * camDirectionality * movementDirectionality;
                desiredRotation = RotationLerp(prevRotation, idealRotation, alpha);
            }
        }

        // Update the target depending on the new desired rotation
        currentTargetPosition = newPlayerPosition + GetCameraOffset(desiredRotation);

        // TODO: Lag boost depending on input override
        // TODO: Prevent collision with environment

        // Update position smoothly
        float posAlpha = (1.0f - cameraTranslationLag);
        transform.position = Vector3.Lerp(transform.position, currentTargetPosition, posAlpha);

        // Target rotation is looking at the player from our new position
        Vector3 lookAtPlayer = Quaternion.LookRotation(newPlayerPosition - transform.position, Vector3.up).eulerAngles;
        currentTargetRotation = new Vector3(lookAtPlayer.x, lookAtPlayer.y, 0);

        // Update rotation smoothly
        float rotAlpha = (1.0f - cameraRotationLag);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(currentTargetRotation), rotAlpha);

        // Update internal variables
        previousPlayerPosition = newPlayerPosition;
        inputOverride = Mathf.Clamp01(inputOverride - Time.deltaTime / inputEffectiveTime);
    }

    // Get the offset from the player to the camera for a given pitch / yaw
    private Vector3 GetCameraOffset(Vector2 rotation)
    {
        float pitch = Mathf.Clamp(rotation.x, pitchRange.x, pitchRange.y);
        float yaw = rotation.y;
        float distance = distanceFactor * idealDistanceForPitch.Evaluate(pitch);
        return Quaternion.Euler(pitch, yaw, 0) * Vector3.back * distance;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1.0f, 0.0f, 1.0f);
        Gizmos.DrawSphere(currentTargetPosition, 0.2f);
        Gizmos.DrawLine(transform.position, transform.position + Quaternion.Euler(currentTargetRotation) * Vector3.forward * 2.0f);

        if (target != null)
        {
            Gizmos.color = new Color(1.0f, 1.0f, 1.0f);
            Gizmos.DrawLine(target.position, target.position + averagePlayerDirection * 2.0f);
        }
    }

    // Slerps from a to b according to t, on the horizontal (XZ) plane
    static Vector3 HSlerp(Vector3 a, Vector3 b, float t)
    {
        Vector3 ha = new Vector3(a.x, 0, a.z);
        Vector3 hb = new Vector3(b.x, 0, b.z);

        return Vector3.Slerp(ha, hb, t) + Vector3.up * Mathf.Lerp(a.y, b.y, t);
    }

    // Shortest path lerp between two rotations
    static private Vector2 RotationLerp(Vector2 a, Vector2 b, float t)
    {
        Vector2 diff = b - a;

        diff.x = Mathf.Repeat(diff.x, 360.0f);
        if (diff.x > 180.0f)
        {
            diff.x -= 360.0f;
        }
        diff.y = Mathf.Repeat(diff.y, 360.0f);
        if (diff.y > 180.0f)
        {
            diff.y -= 360.0f;
        }

        return a + diff * t;
    }
}