﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneration : MonoBehaviour
{
    public static LevelGeneration instance { get; private set; }

    // Original room where to start the level generation from
    public LevelRoom startinRoom;

    // List of rooms we can instantiate
    public GameObject[] possibleRooms;

    // List of bridges between rooms we can spawn
    public GameObject[] possibleBridges;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one LevelGeneration object!");
            Destroy(instance.gameObject);
        }

        instance = this;

        if (possibleRooms.Length == 0)
        {
            Debug.LogError("No room to choose from!");
        }

        if (possibleBridges.Length == 0)
        {
            Debug.LogError("No bridges to choose from!");
        }
    }

    public void ExpandRoom(LevelRoom room)
    {
        if (room == null)
        {
            Debug.LogError("Can't expand null room");
            return;
        }

        foreach (LevelRoomLink exit in room.exits)
        {
            if (exit != null && exit.linkedRoom == null)
            {
                // Choose parameters randomly
                GameObject newRoomPrefab = possibleRooms[Random.Range(0, possibleRooms.Length)];
                GameObject newBridgePrefab = possibleBridges[Random.Range(0, possibleBridges.Length)];

                // Compute the transform for the bridge to be aligned with the room's exit
                LevelBridge bridgeTemplate = newBridgePrefab.GetComponent<LevelBridge>();
                Debug.Assert(bridgeTemplate);
                Debug.Assert(bridgeTemplate.entrance);
                Debug.Assert(bridgeTemplate.exit);

                float exitYaw = exit.transform.rotation.eulerAngles.y;
                float bridgeEntranceLocalYaw = bridgeTemplate.entrance.transform.localEulerAngles.y;
                Quaternion newBridgeRotation = Quaternion.Euler(0, exitYaw - bridgeEntranceLocalYaw, 0);
                Vector3 newBridgePosition = exit.transform.position - newBridgeRotation * bridgeTemplate.entrance.localPosition;

                // Compute the transform for the room to be aligned with the bridge's exit
                LevelRoom roomTemplate = newRoomPrefab.GetComponent<LevelRoom>();
                Debug.Assert(roomTemplate);
                Debug.Assert(roomTemplate.entrance.transform);

                float bridgeExitYaw = newBridgeRotation.eulerAngles.y + bridgeTemplate.exit.localEulerAngles.y;
                float entranceLocalYaw = roomTemplate.entrance.transform.localRotation.eulerAngles.y;
                Quaternion newRoomRotation = Quaternion.Euler(0, bridgeExitYaw - entranceLocalYaw, 0);
                Vector3 newBridgeExitPosition = newBridgePosition + newBridgeRotation * bridgeTemplate.exit.localPosition;
                Vector3 newRoomPosition = newBridgeExitPosition - newRoomRotation * roomTemplate.entrance.transform.localPosition;

                // Spawn objects
                GameObject newBridge = GameObject.Instantiate(newBridgePrefab, newBridgePosition, newBridgeRotation);
                LevelBridge newLevelBridge = newBridge.GetComponent<LevelBridge>();
                Debug.Assert(newLevelBridge);

                GameObject newRoom = GameObject.Instantiate(newRoomPrefab, newRoomPosition, newRoomRotation);
                LevelRoom newLevelRoom = newRoom.GetComponent<LevelRoom>();
                Debug.Assert(newLevelRoom);

                // Check for collisions against pre-existing terrain
                GameObject[] exclusionList = { room.gameObject, newRoom, newBridge };
                if (OverlapLevelGeometry(newLevelBridge, exclusionList) ||
                    OverlapLevelGeometry(newLevelRoom, exclusionList))
                {
                    Destroy(newBridge);
                    Destroy(newRoom);
                    continue;
                }

                // Link the rooms
                exit.linkedRoom = newLevelRoom;
                newLevelRoom.entrance.linkedRoom = room;
            }
        }
    }

    // Check if the given volume overlaps pre-existing geometry if placed at the given transform
    bool OverlapLevelGeometry(LevelGeometry levelGeometry, GameObject[] exclusionList)
    {
        if (levelGeometry != null)
        {
            // Get every overlapping LevelGeometry
            Collider[] collidingObjects = levelGeometry.GetOverlappingObjects(LayerMask.GetMask("LevelGeometry"));

            foreach (Collider other in collidingObjects)
            {
                // Check if the collider is attached to an actual LevelGeometry which isn't to be ignored
                LevelGeometry geo = other.GetComponentInParent<LevelGeometry>();
                if (geo != null && exclusionList != null)
                {
                    bool ignore = false;

                    foreach (GameObject excludedObject in exclusionList)
                    {
                        if (geo.gameObject == excludedObject)
                        {
                            ignore = true;
                            break;
                        }
                    }

                    if (!ignore)
                    {
                        // We overlap some actual LevelGeometry which isn't in the exclusion list
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
