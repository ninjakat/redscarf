﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class SpawnPoint
{
    public Transform point;
    public GameObject template;
}

// We don't have nested prefabs yet so, use custom spawner
public class LevelObjectSpawner : MonoBehaviour
{
    [Tooltip("Automatically spawn on start?")]
    public bool autoSpawn = true;

    public SpawnPoint[] spawnPoints;

    List<GameObject> spawnees = null;
    bool hasSpawned = false;

    virtual protected void Start()
    {
        if (autoSpawn)
        {
            SpawnAll();
        }
    }

    // Spawns all the game objects, returns a list of the objects spawned
    virtual public List<GameObject> SpawnAll()
    {
        if (!hasSpawned)
        {
            spawnees = new List<GameObject>();
            foreach (SpawnPoint spawn in spawnPoints)
            {
                if (spawn != null && spawn.point != null && spawn.template != null)
                {
                    GameObject spawnee = GameObject.Instantiate(spawn.template, spawn.point.position, spawn.point.rotation);
                    spawnees.Add(spawnee);
                }
            }
            hasSpawned = true;
        }

        return spawnees;
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        bool childSelected = false;
        if (Selection.activeObject != null)
        {
            foreach (SpawnPoint spawn in spawnPoints)
            {
                if (spawn != null && spawn.point != null && spawn.point.gameObject == Selection.activeObject)
                {
                    childSelected = true;
                    break;
                }
            }
        }

        if (childSelected)
        {
            Gizmos.matrix = Matrix4x4.identity;
            OnDrawGizmosSelected();
        }
    }

    void OnDrawGizmosSelected()
    {
        if (hasSpawned)
        {
            // Stop rendering once we spawned the objects
            return;
        }

        Gizmos.color = Color.black;
        foreach (SpawnPoint spawn in spawnPoints)
        {
            if (spawn != null && spawn.point != null && spawn.template != null)
            {
                foreach (MeshFilter meshFilter in spawn.template.GetComponentsInChildren<MeshFilter>())
                {
                    if (meshFilter.mesh != null)
                    {
                        Gizmos.DrawWireMesh(meshFilter.mesh, spawn.point.position, spawn.point.rotation, meshFilter.transform.lossyScale);
                    }
                }

                foreach (SkinnedMeshRenderer smr in spawn.template.GetComponentsInChildren<SkinnedMeshRenderer>())
                {
                    if (smr.sharedMesh != null)
                    {
                        Vector3 scale;
                        if (smr.rootBone != null)
                        {
                            scale = smr.rootBone.lossyScale;
                        }
                        else
                        {
                            scale = smr.transform.lossyScale;
                        }
                        Gizmos.DrawWireMesh(smr.sharedMesh, spawn.point.position, spawn.point.rotation, scale);
                    }
                }
            }
        }
    }
#endif
}
