﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class LevelGeometry : MonoBehaviour
{
    public Collider boundingVolume;
    public bool showBoundingVolume = false;

    void Start()
    {
        NavMeshSurface navMeshSurf = GetComponent<NavMeshSurface>();
        if (navMeshSurf != null)
        {
            navMeshSurf.BuildNavMesh();
        }
    }

    public Collider[] GetOverlappingObjects(int layerMask = -1)
    {
        if (boundingVolume != null)
        {
            if (boundingVolume.GetType() == typeof(BoxCollider))
            {
                BoxCollider box = (BoxCollider)boundingVolume;
                Vector3 position = box.transform.TransformPoint(box.center);
                Vector3 size = Vector3.Scale(box.transform.lossyScale, box.size);

                return Physics.OverlapBox(position, size * 0.5f, transform.rotation, layerMask);
            }
            else if (boundingVolume.GetType() == typeof(SphereCollider))
            {
                SphereCollider sphere = (SphereCollider)boundingVolume;

                Vector3 position = sphere.transform.TransformPoint(sphere.center);
                Vector3 size = sphere.transform.lossyScale * sphere.radius;
                float radius = Mathf.Max(Mathf.Max(size.x, size.y), size.z);

                return Physics.OverlapSphere(position, radius);
            }
            else
            {
                Debug.LogError("Unhandled collider type " + boundingVolume.GetType());
            }
        }

        return new Collider[0];
    }

    virtual protected void OnDrawGizmos()
    {
        if (showBoundingVolume)
        {
            Gizmos.color = new Color(1f, 1f, 1f, 0.5f);

            if (boundingVolume.GetType() == typeof(BoxCollider))
            {
                BoxCollider box = (BoxCollider)boundingVolume;

                Vector3 position = box.transform.TransformPoint(box.center);
                Vector3 size = Vector3.Scale(box.transform.lossyScale, box.size);

                Matrix4x4 matrix = new Matrix4x4();
                matrix.SetTRS(position, box.transform.rotation, size);
                Gizmos.matrix = matrix;

                Gizmos.DrawCube(Vector3.zero, Vector3.one);
            }
            else if (boundingVolume.GetType() == typeof(SphereCollider))
            {
                SphereCollider sphere = (SphereCollider)boundingVolume;

                Vector3 position = sphere.transform.TransformPoint(sphere.center);
                Vector3 size = sphere.transform.lossyScale * sphere.radius;
                float radius = Mathf.Max(Mathf.Max(size.x, size.y), size.z);

                Matrix4x4 matrix = new Matrix4x4();
                matrix.SetTRS(position, sphere.transform.rotation, radius * Vector3.one);
                Gizmos.matrix = matrix;

                Gizmos.DrawSphere(Vector3.zero, 1.0f);
            }
        }
    }
}
