﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[DisallowMultipleComponent]
public class LevelBridge : LevelGeometry
{
    public Transform entrance;
    public Transform exit;

#if UNITY_EDITOR
    override protected void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        bool childSelected = false;
        if (Selection.activeObject != null)
        {
            if (entrance != null && entrance.gameObject == Selection.activeObject)
            {
                childSelected = true;
            }
            else if (exit != null && exit.gameObject == Selection.activeObject)
            {
                childSelected = true;
            }
        }

        if (childSelected)
        {
            Gizmos.matrix = Matrix4x4.identity;
            OnDrawGizmosSelected();
        }
    }

    void OnDrawGizmosSelected()
    {
        if (entrance != null)
        {
            Gizmos.color = new Color(0, 0, 0.7f, 0.7f);
            Gizmos.DrawSphere(entrance.position, 0.5f);

            Vector3 fwd = entrance.forward;
            Gizmos.DrawRay(entrance.position, new Vector3(fwd.x, 0, fwd.z).normalized * 2.0f);
        }

        if (exit != null)
        {
            Gizmos.color = new Color(0.7f, 0, 0, 0.7f);
            Gizmos.DrawSphere(exit.position, 0.5f);

            Vector3 fwd = exit.transform.forward;
            Gizmos.DrawRay(exit.transform.position, new Vector3(fwd.x, 0, fwd.z).normalized * 2.0f);
        }
    }
#endif
}