﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class LevelRoomLink
{
    public Transform transform;
    public LevelRoom linkedRoom;
};

[DisallowMultipleComponent]
public class LevelRoom : LevelGeometry
{
    public LevelRoomLink entrance;
    public LevelRoomLink[] exits;

    public bool isExpanded { get; private set; }

    public void Expand()
    {
        if (isExpanded)
        {
            // We already tried that room, don't try again
            return;
        }

        if (LevelGeneration.instance)
        {
            LevelGeneration.instance.ExpandRoom(this);
            isExpanded = true;
        }
        else
        {
            Debug.LogError("No LevelGeneration found, can't expand room!");
        }
    }

#if UNITY_EDITOR
    override protected void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        bool childSelected = false;
        if (Selection.activeObject != null)
        {
            if (entrance.transform != null && entrance.transform.gameObject == Selection.activeObject)
            {
                childSelected = true;
            }
            else
            {
                foreach (LevelRoomLink exit in exits)
                {
                    if (exit != null && exit.transform != null && exit.transform.gameObject == Selection.activeObject)
                    {
                        childSelected = true;
                        break;
                    }
                }
            }
        }

        if (childSelected)
        {
            Gizmos.matrix = Matrix4x4.identity;
            OnDrawGizmosSelected();
        }
    }

    void OnDrawGizmosSelected()
    {
        if (entrance.transform != null)
        {
            Gizmos.color = new Color(0, 0, 0.7f, 0.7f);
            Gizmos.DrawSphere(entrance.transform.position, 0.5f);

            Vector3 fwd = entrance.transform.forward;
            Gizmos.DrawRay(entrance.transform.position, new Vector3(fwd.x, 0, fwd.z).normalized * 2.0f);
        }

        foreach (LevelRoomLink exit in exits)
        {
            if (exit != null && exit.transform != null)
            {
                Gizmos.color = new Color(0.7f, 0, 0, 0.7f);
                Gizmos.DrawSphere(exit.transform.position, 0.5f);

                Vector3 fwd = exit.transform.forward;
                Gizmos.DrawRay(exit.transform.position, new Vector3(fwd.x, 0, fwd.z).normalized * 2.0f);
            }
        }
    }
#endif
}
