﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelRoomExpander : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        LevelRoom room = collision.gameObject.GetComponentInParent<LevelRoom>();
        if (room != null)
        {
            LevelEnemySpawner spawner = room.GetComponent<LevelEnemySpawner>();
            if (spawner != null && spawner.numEnemiesLeft > 0)
            {
                spawner.OnAllEnemiesDead += room.Expand;
            }
            else
            {
                room.Expand();
            }
        }
    }
}
