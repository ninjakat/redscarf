﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEnemySpawner : LevelObjectSpawner
{
    public EnemyPatrolArea patrolArea;
    public delegate void AllEnemiesDeadEvent();
    public event AllEnemiesDeadEvent OnAllEnemiesDead;

    public int numEnemiesLeft { get; private set; }

    override protected void Start()
    {
        numEnemiesLeft = 0;
        base.Start();
    }

    void OnEnemyDeath()
    {
        Debug.Assert(numEnemiesLeft > 0);

        --numEnemiesLeft;
        if (numEnemiesLeft == 0 && OnAllEnemiesDead != null)
        {
            OnAllEnemiesDead();
        }
    }

    override public List<GameObject> SpawnAll()
    {
        if (patrolArea == null)
        {
            patrolArea = GetComponent<EnemyPatrolArea>();
        }

        numEnemiesLeft = 0;
        List<GameObject> spawnedObjects = base.SpawnAll();
        foreach (GameObject spawnedObject in spawnedObjects)
        {
            if (spawnedObject != null)
            {
                EnemyBehaviour enemyBehaviour = spawnedObject.GetComponent<EnemyBehaviour>();
                if (enemyBehaviour != null)
                {
                    enemyBehaviour.patrolArea = patrolArea;

                    DamageComponent damageComponent = enemyBehaviour.GetComponent<DamageComponent>();
                    if (damageComponent != null)
                    {
                        damageComponent.OnDeath += OnEnemyDeath;
                        ++numEnemiesLeft;
                    }
                }
            }
        }

        return spawnedObjects;
    }
}
