﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacter : Character
{
    public void Awake()
    {
        Faction = FactionEnum.Enemy;
    }
}
