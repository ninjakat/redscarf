﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FactionEnum
{
    Player,
    Enemy,
    Neutral
}

public class Character : MonoBehaviour
{
    public FactionEnum Faction { get; protected set; }
}
