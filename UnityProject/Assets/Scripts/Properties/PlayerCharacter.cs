﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : Character
{
    public void Awake()
    {
        Faction = FactionEnum.Player;
    }
}
