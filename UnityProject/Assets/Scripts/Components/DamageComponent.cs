using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageComponent : MonoBehaviour
{
    public float Health = 20;

    // Computes the damage value
    public delegate float TakeDamageDelegate(GameObject other, float damage);
    public TakeDamageDelegate TakeDamage;

    // Notifies any listener we took damage
    public delegate void DamageEvent(GameObject causer, float damageTaken);
    public event DamageEvent OnDamageTaken;

    // Notifies any listener we died
    public delegate void DeathEvent();
    public event DeathEvent OnDeath;

    public bool isDead { get; private set; }

    private void Awake()
    {
        isDead = (Health <= 0);
    }

    public void ApplyDamage(GameObject other, float damage)
    {
        if (TakeDamage == null)
        {
            return;
        }

        if (isDead)
        {
            return;
        }

        float damageTaken = TakeDamage(other, damage);

        Health -= damageTaken;
        if (OnDamageTaken != null)
        {
            OnDamageTaken(other, damageTaken);
        }

        if (Health <= 0)
        {
            isDead = true;
            if (OnDeath != null)
            {
                OnDeath();
            }
        }
    }
}
