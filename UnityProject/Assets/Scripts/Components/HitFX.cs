﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitFX : MonoBehaviour
{
    public Color hitColor = Color.white;
    public float hitDuration = 0.1f;

    List<Material> materials = new List<Material>();
    bool flashing = false;
    float lastDamageTimestamp;
    const string emissionColor = "_EmissionColor";
    const string emissionKeyword = "_EMISSION";

    void OnEnable()
    {
        // Gather materials
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            foreach (Material m in renderer.materials)
            {
                if (m != null)
                {
                    if (m.IsKeywordEnabled(emissionKeyword))
                    {
                        Debug.LogWarning("Material " + m.name + " on object " + renderer.gameObject +
                            " is already using emission. HitFX script will mess it up.");
                    }

                    m.EnableKeyword(emissionKeyword);
                    materials.Add(m);
                }
            }
        }

        // Check we have some materials
        if (materials.Count == 0)
        {
            Debug.LogWarning("HitFX didn't find any material on " + gameObject.name);
            return;
        }

        // Register for hits
        DamageComponent dmgComp = GetComponentInChildren<DamageComponent>();
        if (dmgComp == null)
        {
            Debug.LogWarning("HitFX couldnt' find a DamageComponent on " + gameObject.name);
            return;
        }
        dmgComp.OnDamageTaken += OnDamage;
    }

    private void OnDisable()
    {
        DamageComponent dmgComp = GetComponentInChildren<DamageComponent>();
        if (dmgComp != null)
        {
            dmgComp.OnDamageTaken -= OnDamage;
        }
    }

    void OnDamage(GameObject causer, float damageTaken)
    {
        if (damageTaken > 0)
        {
            // Reset timestamp
            lastDamageTimestamp = Time.time;

            // Run coroutine if it's not already running
            if (!flashing)
            {
                StartCoroutine(Flash());
            }
        }
    }

    IEnumerator Flash()
    {
        // Flash on
        flashing = true;
        foreach (Material m in materials)
        {
            if (m != null)
            {
                m.SetColor(emissionColor, hitColor);
            }
        }

        // Wait for duration
        yield return new WaitUntil(() => (Time.time - lastDamageTimestamp) > hitDuration);
        
        // Flash off
        foreach (Material m in materials)
        {
            if (m != null)
            {
                m.SetColor(emissionColor, Color.black);
            }
        }
        flashing = false;
    }
}