﻿using UnityEngine;
using System.Collections;

public class ScreenLog : MonoBehaviour
{
    public float messageTime = 1.0f;

    ArrayList logList = new ArrayList();

    struct LogMessage
    {
        public LogMessage(string message, float timestamp)
        {
            this.message = message;
            this.timestamp = timestamp;
        }

        public string message;
        public float timestamp;
    }

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        logList.Add(new LogMessage(logString, Time.time));
    }

    void OnGUI()
    {
        float now = Time.time;
        string log = string.Empty;

        for (int i = logList.Count - 1; i >= 0; --i)
        {
            LogMessage message = (LogMessage)logList[i];

            if (now - message.timestamp < messageTime)
            {
                log = message.message + "\n" + log;
            }
            else
            {
                logList.RemoveAt(i);
            }
        }

        GUILayout.Label(log);
    }
}

