﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 3.0f;
    public float damage = 1.0f;

    private void Update()
    {
        GetComponent<Rigidbody>().MovePosition(transform.position + transform.forward * speed * Time.deltaTime);
    }

    public void Disappear()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        DamageComponent dmgComp = other.gameObject.GetComponent<DamageComponent>();
        if (dmgComp != null)
        {
            dmgComp.ApplyDamage(gameObject, damage);
        }
        
        Disappear();
    }
}
